

import Foundation

struct ErrorResponse: Decodable {
  let cod: String
  let message: String
}
